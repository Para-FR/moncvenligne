<?php

/***
 * Conditions
 */

$mavariable = 'Autorisé';

if ($mavariable === 'Autorisé') {
    echo 'Je suis autorisé à passer';
} else {
    echo 'Accès Interdit';
}

/***
 * Conditions Multiples
 */

$autorisation = true; // String
$age = 15; // Int

// die(var_dump($autorisation));

if ($autorisation === 'Autorisé' && $age >= 18) {
    echo 'Tu es autorisé à passer et tu es majeur';
} elseif ($autorisation === 'Autorisé' || $age >= 18) {
        echo 'Vous êtes autorisé à passer mais vous êtes mineur';
} else {
    echo 'Vous ne correspondez pas aux critères : Accès refusé';
}
?>