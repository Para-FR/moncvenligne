<?php

// $id = 0;

/*while ($id < 11) {
    echo 'Mon pokédex contient ' . $id++ . ' Pokémons<br>';
}*/

$pokedex = array(
    array(
        'id' => 0,
        'pok_name' => 'Pickachu',
        'pok_type' => 'Electrique',
        'pok_hp' => 99,
        'pok_weight' => 6,
        'pok_height' => 0.4,
        'pok_description' => 'Les Pikachu se disent bonjour en se frottant la queue et en y faisant passer du courant électrique.',
        'pok_img' => 'https://www.pokepedia.fr/images/thumb/e/e7/Pikachu-RFVF.png/250px-Pikachu-RFVF.png'
    ),
    array(
        'id' => 1,
        'pok_name' => 'Bulbizarre',
        'pok_type' => 'Plante',
        'pok_hp' => 120,
        'pok_weight' => 6,
        'pok_height' => 0.4,
        'pok_description' => 'Description bulbizarre',
        'pok_img' => 'https://www.pokepedia.fr/images/thumb/e/ef/Bulbizarre-RFVF.png/250px-Bulbizarre-RFVF.png'
    ),
    array(
        'id' => 2,
        'pok_name' => 'Salamèche',
        'pok_type' => 'Feu',
        'pok_hp' => 90,
        'pok_weight' => 6,
        'pok_height' => 0.4,
        'pok_description' => 'Description de Salamèche',
        'pok_img' => 'https://www.pokepedia.fr/images/thumb/8/89/Salam%C3%A8che-RFVF.png/250px-Salam%C3%A8che-RFVF.png'

    ),
    array(
        'id' => 3,
        'pok_name' => 'Carapuce',
        'pok_type' => 'Eau',
        'pok_hp' => 100,
        'pok_weight' => 6,
        'pok_height' => 0.4,
        'pok_description' => 'Description Carapuce',
        'pok_img' => 'https://www.pokepedia.fr/images/thumb/c/cc/Carapuce-RFVF.png/250px-Carapuce-RFVF.png'

    )
);

//echo $pokedex[0];

//die(var_dump($pokedex[0]['id']));

/*for ($id = 1; $id <= 4; $id++) {
    echo 'Votre pokémon n°' . $id++ . ' est ' . $pokedex[$id] . '<br>';
}*/


