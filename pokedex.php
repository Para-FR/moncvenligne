<?php require('components/header.php')?>
<!-- </Head -->
<div class="container">
    <div class="row">
        <div class="col-12">
            <h1 class="text-center text-danger">Contenu de mon Pokédex</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <table class="table table-hover table-dark text-center">
                <thead>
                <tr>
                    <th >#</th>
                    <th>Image</th>
                    <th>Nom</th>
                    <th>Fiche</th>
                </tr>
                </thead>
                <tbody>
                <?php for ($i = 0; $i <= 3; $i++) { ?>
                <tr>
                    <th><?= $pokedex[$i]['id'] ?></th>
                    <td><img class="img-fluid" style="height: 50px" src="<?= $pokedex[$i]['pok_img'] ?>" alt=""></td>
                    <td><?= $pokedex[$i]['pok_name'] ?></td>
                    <td><a class="btn btn-green" href="pokemon_profile.php?id=<?= $pokedex[$i]['id'] ?>">Voir la Fiche</a></td>
                </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

</body>

</html>
