<?php
    require ('boucles.php');

    //die(var_dump($_GET['id']));

    if ($_GET['id'] === '0') {
        $id = $_GET['id'];
        $pokemonSelected = $pokedex[$id];
    } elseif ($_GET['id'] && $_GET['id'] && $_GET['id'] <= count($pokedex) && is_numeric($_GET['id'])) {
        $id = $_GET['id'];
        $pokemonSelected = $pokedex[$id];
    }
    else {
        header("Location: http://localhost/moncvenligne/pokedex.php");
    }
    // die(var_dump($pokedex));
//var_dump($pokemonSelected);
//var_dump($_GET);
?>
<!doctype html>
<html lang="en">

<!-- Head -->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="#">

    <title>Profil</title>
    <!-- Bootstrap core CSS -->
    <link href="./assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link rel="stylesheet" href="./assets/css/commun.css">
</head>
<!-- </Head -->
<body>
<div class="container">
    <div class="row">
        <div class="col-12">
            <h1 class="text-center text-danger">Profil du Pokémon <?= $pokemonSelected['pok_name'] ?></h1>
        </div>
    </div>
    <div class="row">
        <div class="col-6">
            <div class="card mb-3">
                <div class="card-header bg-grey-dark-plus">
                    <h5 class="card-title text-center mt-2 text-green">Image</h2></h5>
                </div>
                <img src="<?= $pokemonSelected['pok_img']  ?>" class="card-img-top" alt="...">
            </div>
        </div>
        <div class="col-6">
            <div class="card">
                <div class="card-header bg-grey-dark-plus">
                    <h5 class="card-title text-center mt-2 text-green">Informations</h2></h5>
                </div>
                <div class="card-body bg-grey-dark">
                    <ul class="list-group">
                        <li class="list-group-item d-flex justify-content-between align-items-center">
                            Type
                            <span class="badge badge-primary badge-pill bg-green"><?= $pokemonSelected['pok_type'] ?></span>
                        </li>
                        <li class="list-group-item d-flex justify-content-between align-items-center">
                            HP
                            <span class="badge badge-primary badge-pill bg-green"><?= $pokemonSelected['pok_hp'] ?></span>
                        </li>
                        <li class="list-group-item d-flex justify-content-between align-items-center">
                            Poids
                            <span class="badge badge-primary badge-pill bg-green"><?= $pokemonSelected['pok_weight'] ?>Kg</span>
                        </li>
                        <li class="list-group-item d-flex justify-content-between align-items-center">
                            Taille
                            <span class="badge badge-primary badge-pill bg-green"><?= $pokemonSelected['pok_height'] ?>m</span>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="card mb-3">
                <div class="card-header bg-grey-dark-plus">
                    <h5 class="card-title text-center mt-2 text-green">Description</h2></h5>
                </div>
                <div class="card-body bg-grey-dark">
                    <p class="mt-2 text-white">
                        <?= $pokemonSelected['pok_description'] ?>
                    </p>
                </div>
            </div>
            <a href="http://localhost/moncvenligne/pokedex.php" class="btn btn-green btn-lg">Retour</a>
        </div>
    </div>
</div>

</body>

</html>
