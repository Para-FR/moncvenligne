<?php

include "components/header.php";
//die(var_dump($_GET));
// die(var_dump(count($pokedex)));
if ($_GET['id'] === '0') {
    $id = $_GET['id'];
    $pokemonSelected = $pokedex[$id];
} else if (isset($_GET['id']) && $_GET['id'] && $_GET['id'] <= count($pokedex) && is_numeric($_GET['id'])) {

    $id = $_GET['id'];
//die(var_dump($pokedex));
    $pokemonSelected = $pokedex[$id];
} else {
    header("Location: pokedex.php");
}
 // die(var_dump($pokemonSelected));
?>

<div class="container">
    <div class="row mt-2">
        <div class="col-12">
            <h1 class="text-center text-danger">Profil du Pokémon <?= $pokemonSelected['pok_name'] ?></h1>
        </div>
    </div>
    <div class="row">
        <div class="col-6">
            <div class="card">
                <div class="card-header bg-grey-dark-plus">
                    <h5 class="card-title text-center text-green">Image</h5>
                </div>
                <img src="<?= $pokemonSelected['pok_img'] ?>" class="card-img-top" alt="...">
            </div>
        </div>
        <div class="col-6">
            <div class="card">
                <div class="card-header bg-grey-dark-plus">
                    <h5 class="card-title text-center text-green">Statistiques</h5>
                </div>
                <div class="card-body">
                    <ul class="list-group">
                        <li class="list-group-item d-flex justify-content-between align-items-center">
                            Type
                            <span class="badge badge-primary badge-pill"><?= $pokemonSelected['pok_type'] ?></span>
                        </li>
                        <li class="list-group-item d-flex justify-content-between align-items-center">
                            HP
                            <span class="badge badge-primary badge-pill"><?= $pokemonSelected['pok_hp'] ?></span>
                        </li>
                        <li class="list-group-item d-flex justify-content-between align-items-center">
                            Taille
                            <span class="badge badge-primary badge-pill"><?= $pokemonSelected['pok_height'] ?></span>
                        </li>
                        <li class="list-group-item d-flex justify-content-between align-items-center">
                            Poids
                            <span class="badge badge-primary badge-pill"><?= $pokemonSelected['pok_weight'] ?></span>
                        </li>
                    </ul>
                </div>
                <div class="card-header bg-grey-dark-plus">
                    <h5 class="card-title text-center text-green">Description</h5>
                </div>
                <div class="card-body">
                    <p><?= $pokemonSelected['pok_description'] ?></p>
                </div>
            </div>
            <a href="pokedex.php" class="btn btn-green btn-lg mt-2 float-right">Retour</a>
        </div>
    </div>
</div>
